#!/bin/bash

set -e

echo "GTG# compiling svelte"
cd /projet/wol-svelte
npm run build

echo "GTG# Copying resources"
rm -f /projet/wol-svelte/src/WebContent/project/final/images/*
cp /projet/wol-svelte/src/WebContent/project/img/* /projet/wol-svelte/src/WebContent/project/final/images/
