package ch.gobothegeek.tech.wol.web;

import ch.gobothegeek.tech.wol.controllers.WolController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

// base url for all CRUD methods
@ApplicationPath("/wol")
public class WolApplication extends Application {
	private final Logger logger = LoggerFactory.getLogger(WolApplication.class);

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(WolController.class);
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<>();
        return singletons;
    }
}

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-svelte".

"WOL-svelte" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-svelte" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-svelte".  If not, see <https://www.gnu.org/licenses/>.
*/