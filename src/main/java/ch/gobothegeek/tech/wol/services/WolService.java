package ch.gobothegeek.tech.wol.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.*;

@RequestScoped
public class WolService {
	private final Logger logger = LoggerFactory.getLogger(WolService.class);

	private static final int WOL_PORT = 40000;
	private static final int MAGIC_SIZE_TOTAL = 102; // (6 + 16 * 6 bytes)
	private static final int MAGIC_SIZE_FF = 6; // 6 0xFF bytes
	private static final int MAGIC_REPEAT_MAC = 16; // 16 times repetitions
	private static final int MAGIC_SIZE_MAC = 6; // mac address need 6 bytes
	private static final String IP_BROADCAST = "255.255.255.255";


	@Inject	private HttpServletRequest request;

	public String getRemoteAddress() {
		return request.getRemoteAddr();
	}

	// generate a magic packet for waking up a computer
	// packet is an array of 102 bytes (6 + 16 * 6) with this content:
	// 6 first bytes are 0xFF
	// then mac address is repeated 16 times
	// Ex: for mac { 12:23:34:45:56:78 }, packet is:
	// [ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	//   0x12, 0x23, 0x34, 0x45, 0x56, 0x78,
	//   0x12, 0x23, 0x34, 0x45, 0x56, 0x78, ... ]
	private byte[] createMagicPacket(String mac) {
		String[] hexMac;
		byte[] packet = new byte[MAGIC_SIZE_TOTAL];

		// split mac address
		hexMac = mac.split(":");
		// add 0xFF 6 times
		for (int ffCount = 0; ffCount < MAGIC_SIZE_FF; ffCount++) {
			packet[ffCount] = (byte)0xFF;
		}
		// repeat mac address 16 times
		for (int macCnt = 0; macCnt < MAGIC_REPEAT_MAC; macCnt++) {
			for (int hexCnt = 0; hexCnt < MAGIC_SIZE_MAC; hexCnt++) {
				packet[MAGIC_SIZE_FF + macCnt * MAGIC_SIZE_MAC + hexCnt] = (byte)Integer.valueOf( hexMac[hexCnt], 16 ).intValue();
			}
		}
		return packet;
	}

	public boolean sendPacket(String mac) {
		InetAddress ipAddr;
		byte[] magic;
		DatagramPacket packet;
		DatagramSocket socket;

		try {
			// log who ask for a wake up
			logger.info("Wake up of [" + mac + "] required from IP [" + this.getRemoteAddress() + "]");
			System.out.println("Wake up of [" + mac + "] required from IP [" + this.getRemoteAddress() + "]");
			// transform string to IP address
			ipAddr = InetAddress.getByName(IP_BROADCAST);
			// generate magic packet
			magic = this.createMagicPacket(mac);
			// prepare UDP packet
			packet = new DatagramPacket(magic, magic.length, ipAddr, WOL_PORT);
			// open socket
			socket = new DatagramSocket();
			// send packet
			socket.send(packet);
			// close socket
			socket.close();
			// done
			return true;
		} catch (UnknownHostException e) {
			logger.error("Unknown IP address", e);
			e.printStackTrace();
		} catch (SocketException e) {
			logger.error("Unable to open socket", e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Unable to send magic packet", e);
			e.printStackTrace();
		}
		// an error has occured
		return false;
	}
}

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-svelte".

"WOL-svelte" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-svelte" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-svelte".  If not, see <https://www.gnu.org/licenses/>.
*/