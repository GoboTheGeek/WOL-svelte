package ch.gobothegeek.tech.wol.controllers;

import ch.gobothegeek.tech.wol.model.json.WolRequest;
import ch.gobothegeek.tech.wol.model.json.WolResponse;
import ch.gobothegeek.tech.wol.services.WolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

// Manage access point in computer package
@RequestScoped
@Path("/computer")
@Named
public class WolController {
	private final Logger logger = LoggerFactory.getLogger(WolController.class);

	@Inject private WolService wolService;

	// url to wake up a computer
	@POST
	@Path("/wakeup")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response wakeUpComputer(WolRequest wolParams) {
		WolResponse wolResponse = new WolResponse(false);

		wolResponse.setWakeSent(this.wolService.sendPacket(wolParams.getMac()));
		// return response
		return Response.status(Response.Status.OK).entity(wolResponse).build();
	}
}

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-svelte".

"WOL-svelte" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-svelte" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-svelte".  If not, see <https://www.gnu.org/licenses/>.
*/