// This class display a static list of wakeable computers.
// computers are configured in "configuration.js" file.
import GtgUtilsWeb from '../../frameworks/gtg-svelte/gtg-utils-web.js';
import CONFIG from '../js/configuration.js';

class ComputerUI {
    constructor(user, mac, position) {
        this.user = user; // username of computer (so user can find easily its computer)
        this.mac = mac; // mac address of computer (only way to use WOL)

        this.cssColor = { state: true, on: (0 === position % 2? 'btn-outline-info' : 'btn-outline-primary'), off: 'btn-outline-light disabled' }; // css class to apply to button
        this.cssIcon = { state: true, on: 'fa-computer', off: 'fa-ban' }; // css class to apply to button's icon
    };

    // activate or deactivate button
    activate(search) {
        this.cssColor.state = (this.user.toLowerCase().startsWith(search.toLowerCase()));
        this.cssIcon.state = (this.user.toLowerCase().startsWith(search.toLowerCase()));
    };

    // getters
    get userName() { return this.user; };
    get macAddr() { return this.mac; };
    get color() { return (this.cssColor.state? this.cssColor.on : this.cssColor.off); };
    get icon() { return (this.cssIcon.state? this.cssIcon.on : this.cssIcon.off); };
};

export default ComputerUI;

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-svelte".

"WOL-svelte" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-svelte" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-svelte".  If not, see <https://www.gnu.org/licenses/>.
*/