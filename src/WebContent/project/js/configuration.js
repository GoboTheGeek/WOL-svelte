let CONFIG = {
    computers: [
        { user: 'NAS', mac: '00:11:32:3e:cd:40' }
    ],
    urls: {
        computers: '/wolsv/spa/computers',
        wakeup: '/wolsv/spa/wakeup',
        success: '/wolsv/spa/success',
        error: '/wolsv/spa/error',
        wol: '/wolsv/wol/computer/wakeup'
    }
};

export default CONFIG;

/*
Copyright 2022 Gobo the Geek

This file is part of "WOL-svelte".

"WOL-svelte" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"WOL-svelte" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "WOL-svelte".  If not, see <https://www.gnu.org/licenses/>.
*/