# WOL-svelte

## En français

Wake On LAN: affiche une liste statique d'ordinateurs configurés pour le "wake on lan".

Cette version repose sur Bootstrap 5, Font awesome 6 et Svelte 3 pour le front end. De son côté, le back end repose sur Apache Deltaspike v1.9 et fonctionne sous Apache TomEE 8.

## English version

Wake On LAN: displays a static list of wakeable computers.

This version uses Bootstrap 5, Font awesome 6 and Svelte 3 for front end. Back end uses Apache Deltaspike 1.9 and TomEE 8.
